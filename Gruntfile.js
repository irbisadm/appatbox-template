module.exports = function (grunt) {
  grunt.initConfig({
    clean: {
      dist: ['build/*','tmp/*']
    },
    copy:{
      svg:{
        files:[
          {expand: true, flatten: true, src: ['src/svg/**'], dest: 'build/assets/svg/', filter: 'isFile'}
        ]
      },
      audio:{
        files:[
          {expand: true, flatten: true, src: ['src/audio/**'], dest: 'build/assets/audio/', filter: 'isFile'}
        ]
      }
    },
    postcss: {
      options: {
        map: {
          inline: false,
          annotation: 'build/assets/map/'
        },
        processors: [
          require('postcss-nested'),
          require('pixrem')(),
          require('autoprefixer')({browsers: 'last 3 versions'}),
          require('cssnano')()
        ]
      },
      dist: {
        src: ['tmp/css/style.pcss'],
        dest: 'build/assets/css/style.css'
      }
    },
    pug: {
      compile: {
        options: {
          pretty: true
        },
        files: [{
          expand: true,
          cwd: './src/pug/',
          src: "**/*.pug",
          ext: '.html',
          dest: "./build/"
        }]
      }
    },
    htmlmin: {
      prod: {
        options: {
          removeComments: true,
          collapseWhitespace: true
        },
        files: [{
          expand: true,
          cwd: './build/',
          src: '*.html',
          dest: "./build/"
        }]
      }
    },
    concat: {
      js: {
        src: ['src/js/*.js'],
        dest: 'tmp/js/pack.es2015.js'
      },
      css:{
        src:['src/css/*.pcss'],
        dest:'tmp/css/style.pcss'
      }
    },
    babel: {
      options: {
        sourceMap: true
      },
      dist: {
        files: [{
          expand: true,
          cwd: './tmp/js/',
          src: "**/*.js",
          ext: '.js',
          dest: "./build/assets/js/"
        }]
      }
    },
    uglify: {
      my_target: {
        files: {
          'build/assets/js/pack.min.js': ['build/assets/js/pack.es2015.js']
        }
      }
    },
    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'src/img',
          src: ['**/*.{png,jpg,gif}'],
          dest: 'build/assets/img/'
        }]
      }
    },
    watch: {
      pug: {
        files: ['src/pug/*.pug', 'src/templates/*.pug', 'src/mail/*.pug'],
        tasks: ['pug'],
        options: {
          spawn: false
        }
      },
      postcss: {
        files: ['src/css/*.pcss'],
        tasks: ['concat:css','postcss'],
        options: {
          spawn: false
        }
      },
      img: {
        files: ['src/img/*.png', 'src/img/*.jpg', 'src/img/*.gif'],
        tasks: ['newer:imagemin'],
        options: {
          spawn: false
        }
      },
      static: {
        files: ['src/svg/*','src/audio/*'],
        tasks: ['copy'],
        options: {
          spawn: false
        }
      },
      imgsvg: {
        files: ['src/svg/*.svg'],
        tasks: ['copy:svg'],
        options: {
          spawn: false
        }
      },
      js: {
        files: ['src/js/*.js'],
        tasks: ['concat:js','babel','uglify'],
        options: {
          spawn: false
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-babel');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-pug');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-newer');

  grunt.registerTask('build', ['newer:imagemin', 'pug','copy', 'concat', 'postcss', 'babel', 'uglify']);
  grunt.registerTask('rebuild', ['clean', 'build']);
  grunt.registerTask('default', ['build', 'watch']);
  grunt.registerTask('publish', ['build', 'htmlmin']);
};